using TMPro;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(TextMeshPro))]
public class D2_ScoreManager : MonoBehaviour
{
    public static UnityEvent ScoreChanged = new UnityEvent();
    public static void AddScore()
    {
        s_score++;
        ScoreChanged?.Invoke();
    }
    private static int s_score;
    private TMP_Text m_text;

    private void Start()
    {
        m_text = GetComponent<TMP_Text>();
        UpdateScore();
        ScoreChanged.AddListener(UpdateScore);
    }

    private void OnDestroy()
    {
        ScoreChanged.RemoveListener(UpdateScore);
    }

    private void UpdateScore()
    {
        m_text.text = $"Score: {s_score}";
    }
}
