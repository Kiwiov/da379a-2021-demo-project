using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D2_Bullet : MonoBehaviour
{
    [SerializeField] private float m_speed;
    [SerializeField] private float m_timeToLive;

    private void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(m_speed, 0);
        Destroy(gameObject, m_timeToLive);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
        Destroy(gameObject);
        D2_ScoreManager.AddScore();
    }
}
