using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D2_AstroidSpawner : MonoBehaviour
{
    [SerializeField] private GameObject m_astroidPrefab;
    [SerializeField] private float m_spawnRate;

    private float m_nextSpawn;

    private void Start()
    {
        m_nextSpawn = 0f;
    }

    private void Update()
    {
        while(m_nextSpawn <= 0f)
        {
            float value = Random.Range(0.25f, 1.5f);
            Instantiate(m_astroidPrefab, transform).transform.localScale += new Vector3(value, value, value);
            m_nextSpawn += m_spawnRate;
        }
        m_nextSpawn -= Time.deltaTime;
    }
}
