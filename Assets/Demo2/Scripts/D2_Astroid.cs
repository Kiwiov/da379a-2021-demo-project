using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D2_Astroid : MonoBehaviour
{
    [SerializeField] private float m_speed;

    private Rect m_screenBounds;
    private Vector3 m_movementVector;

    private void Start()
    {
        m_screenBounds = CalculateBounds(0.5f);
        float y = Random.Range(m_screenBounds.yMin, m_screenBounds.yMax);
        transform.position = new Vector3(m_screenBounds.xMax, y, 0);

        m_movementVector = new Vector3(-m_speed, 0, 0);
    }

    private void Update()
    {
        transform.position += m_movementVector * Time.deltaTime;

        if(transform.position.x < m_screenBounds.xMin)
        {
            Destroy(gameObject);
        }
    }

    private Rect CalculateBounds(float objectSize)
    {
        Vector3 cameraPosition = Camera.main.transform.position;
        float ySize = Camera.main.orthographicSize;
        float xSize = ySize * Screen.width / Screen.height;
        Rect r = new Rect
        {
            yMax = ySize + cameraPosition.y - objectSize,
            yMin = -ySize + cameraPosition.y + objectSize,
            xMax = xSize + cameraPosition.x + objectSize,
            xMin = -xSize + cameraPosition.x - objectSize
        };
        return r;
    }
}
