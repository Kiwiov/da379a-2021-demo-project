using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    [SerializeField] Vector3 EulerRotation;
    [SerializeField] bool RandomRotation = false;
    Vector3 rot = Vector3.zero;

    private void Start()
    {
        if (RandomRotation)
        {
            EulerRotation = new Vector3(Random.Range(0, 361), Random.Range(0, 361), Random.Range(0, 361));
        }
    }

    // Update is called once per frame
    void Update()
    {
        rot = (EulerRotation * Time.deltaTime);

        transform.Rotate(rot);
    }
}
