using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class D2_PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject m_bulletPrefab;
    [SerializeField] private float m_fireRate;

    [SerializeField] private float m_verticalSpeed;
    [SerializeField] private float m_horizontalSpeed;
    [SerializeField] private float m_playerSize;

    private float m_yMax;
    private float m_yMin;
    private float m_xMax;
    private float m_xMin;

    private float m_cooldown;
    private void Start()
    {
        CalculateBounds();
        m_cooldown = 0;
    }

    private void Update()
    {
        MovePlayer();
        ClampToScreen();
        Fire();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
        Destroy(gameObject);
    }

    private void CalculateBounds()
    {
        Vector3 cameraPosition = Camera.main.transform.position;
        float ySize = Camera.main.orthographicSize;
        float xSize = ySize * Screen.width / Screen.height;
        m_yMax = ySize + cameraPosition.y - m_playerSize;
        m_yMin = -ySize + cameraPosition.y + m_playerSize;
        m_xMax = xSize + cameraPosition.x - m_playerSize;
        m_xMin = -xSize + cameraPosition.x + m_playerSize;
    }

    private void MovePlayer()
    {
        transform.position += new Vector3(m_horizontalSpeed * Input.GetAxis("Horizontal") * Time.deltaTime, m_verticalSpeed * Input.GetAxis("Vertical") * Time.deltaTime, 0);
    }

    private void ClampToScreen()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, m_xMin, m_xMax), Mathf.Clamp(transform.position.y, m_yMin, m_yMax), 0);
    }

    private void Fire()
    {
        m_cooldown -= Time.deltaTime;
        if(Input.GetButton("Fire1") && m_cooldown <= 0)
        {
            m_cooldown = m_fireRate;
            Instantiate(m_bulletPrefab, transform.position, Quaternion.identity);
        }
    }
}
