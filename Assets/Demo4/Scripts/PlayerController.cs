﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private CharacterController CC;
    [SerializeField] private float speed = 5;

    Vector2 direction = new Vector2();
    Vector3 relative = new Vector3();
    Vector3 mouseposition = new Vector3();
    Vector3 playerposition = new Vector3();
    Vector3 lookDir = new Vector3();

    void Update()
    {
        //Rotate player
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane p = new Plane(Vector3.up, Vector3.zero);
        float dist = 0;
        p.Raycast(ray, out dist);
        mouseposition = ray.GetPoint(dist);
        //if (Physics.Raycast(ray, out RaycastHit hit))
        //{
        //    mouseposition = hit.point;
        //}

        //mouseposition.y = 0;
        playerposition = new Vector3(transform.position.x, 0, transform.position.z);
        lookDir = mouseposition - playerposition;
        lookDir.Normalize();

        transform.rotation = Quaternion.LookRotation(lookDir, Vector3.up);

        //Get movement input
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");

        direction.Normalize();

        //Animate player
        relative = Quaternion.Inverse(transform.rotation) * new Vector3(direction.x, 0, direction.y);

        animator.SetFloat("DirX", relative.x);
        animator.SetFloat("DirY", relative.z);

        animator.SetBool("Moving", direction.sqrMagnitude > 0);

        //Move player
        if (direction.sqrMagnitude > 0)
        {
            Vector3 movement = new Vector3(direction.x, 0, direction.y);
            CC.Move(movement * speed * Time.deltaTime);
        }

        
    }
}
