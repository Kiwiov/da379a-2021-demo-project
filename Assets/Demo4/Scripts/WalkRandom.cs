using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkRandom : MonoBehaviour
{
    [SerializeField] private float m_speed;
    private Animator m_animator;
    private SpriteRenderer m_spriteRenderer;
    private Vector3 m_direction = Vector3.zero;

    void Start()
    {
        m_animator = GetComponent<Animator>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();

        StartCoroutine(ChangeDirection());
    }

    void Update()
    {
        transform.position += m_direction * m_speed * Time.deltaTime;
    }

    private IEnumerator ChangeDirection()
    {
        while (true)
        {
            WaitForSeconds wait = new WaitForSeconds(Random.value * 3);
            m_direction.x = Random.Range(-1, 2);

            //RIGHT
            if (m_direction.x > 0)
            {
                m_animator.SetBool("Walking", true);
                m_spriteRenderer.flipX = true;
            }
            //LEFT
            else if (m_direction.x < 0)
            {
                m_animator.SetBool("Walking", true);
                m_spriteRenderer.flipX = false;
            }
            //IDLE
            else
            {
                m_animator.SetBool("Walking", false);
            }

            yield return wait;
        }
        
    }
}
