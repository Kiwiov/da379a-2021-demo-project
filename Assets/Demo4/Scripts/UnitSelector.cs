﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelector : MonoBehaviour
{
    [SerializeField] RectTransform m_selectionBox;
    private List<GoToPoint> m_units;
    private Vector2 m_firstClick;

    private void Start()
    {
        m_units = new List<GoToPoint>();
        m_units.AddRange(FindObjectsOfType<GoToPoint>());
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            m_selectionBox.gameObject.SetActive(true);
            m_firstClick = Input.mousePosition;
            m_selectionBox.position = m_firstClick;
        }

        if(Input.GetMouseButton(0))
        {
            var mousePos = Input.mousePosition;
            float width = mousePos.x - m_firstClick.x;
            float height = mousePos.y - m_firstClick.y;

            m_selectionBox.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height));
            m_selectionBox.anchoredPosition = m_firstClick + new Vector2(width / 2, height / 2);
        }

        if(Input.GetMouseButtonUp(0))
        {
            m_selectionBox.gameObject.SetActive(false);
            Vector2 min = m_selectionBox.anchoredPosition - (m_selectionBox.sizeDelta / 2);
            Vector2 max = m_selectionBox.anchoredPosition + (m_selectionBox.sizeDelta / 2);

            foreach (var unit in m_units)
            {
                Vector2 pos = Camera.main.WorldToScreenPoint(unit.transform.position);
                if (pos.x >= min.x && pos.x <= max.x && 
                    pos.y >= min.y && pos.y <= max.y)
                {
                    unit.Selected = true;
                }
                else
                {
                    unit.Selected = false;
                }
            }
        }
    }
}
