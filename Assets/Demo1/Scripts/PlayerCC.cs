using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCC : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private bool m_isActive = true;
    [SerializeField] private CharacterController m_characterController;

    [Header("Movement")]
    [SerializeField] private float m_movementSpeed;
    [SerializeField] private float m_gravity;
    [SerializeField] private float m_jumpPower;

    [Header("Ground Check")]
    [SerializeField] private Transform m_groundCheckTransform;
    [SerializeField] private float m_groundCheckRadius;
    [SerializeField] private LayerMask m_groundMask;

    private Vector3 m_verticalMovement = Vector3.zero;
    private bool m_isGrounded = false;

    private void Update()
    {
        if (m_isActive)
        {
            Movement();
            GroundCheck();
            GravityAndJump();
        }
    }

    private void Movement()
    {
        //Get Inputs
        Vector3 movementThisFrame = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        
        //Make Move
        m_characterController.Move(movementThisFrame.normalized * m_movementSpeed * Time.deltaTime);
    }

    private void GroundCheck()
    {
        m_isGrounded = Physics.CheckSphere(m_groundCheckTransform.position, m_groundCheckRadius, m_groundMask);
    }

    private void GravityAndJump()
    {
        //Are we touching ground?
        if (m_isGrounded == false)
        {
            m_verticalMovement.y += -m_gravity * Time.deltaTime;
            
        }
        else
        {
            if (Input.GetButtonDown("Jump"))
            {
                m_verticalMovement.y += m_jumpPower;
            }
            else
            {
                m_verticalMovement.y = 0;
            }
        }

        if (m_verticalMovement.magnitude != 0)
        {
            m_characterController.Move(m_verticalMovement * Time.deltaTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(m_groundCheckTransform.position, m_groundCheckRadius);
    }
}