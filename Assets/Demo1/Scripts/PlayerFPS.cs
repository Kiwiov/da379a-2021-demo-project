using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPS : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private bool m_isActive = true;
    [SerializeField] private CharacterController m_characterController;

    [Header("LookAround")]
    [SerializeField] private Camera m_camera;
    [SerializeField] private float m_mouseSpeed;
    [SerializeField] private bool m_flipY;
    private float mouseYrot = 0;

    [Header("Movement")]
    [SerializeField] private float m_movementSpeed;
    [SerializeField] private float m_gravity;
    [SerializeField] private float m_jumpPower;

    [Header("Ground Check")]
    [SerializeField] private Transform m_groundCheckTransform;
    [SerializeField] private float m_groundCheckRadius;
    [SerializeField] private LayerMask m_groundMask;

    private Vector3 m_verticalMovement = Vector3.zero;
    private bool m_isGrounded = false;

    private void Update()
    {
        if (m_isActive)
        {
            LookAround();
            Movement();
            GroundCheck();
            GravityAndJump();
        }
    }

    private void LookAround()
    {
        //Get Inputs
        float mouseY = Input.GetAxis("Mouse Y") * m_mouseSpeed * Time.deltaTime;
        float mouseX = Input.GetAxis("Mouse X") * m_mouseSpeed * Time.deltaTime;

        //Rotate (Y)
        mouseYrot += (m_flipY ? mouseY : -mouseY);

        mouseYrot = Mathf.Clamp(mouseYrot, -80f, 80f);
        m_camera.transform.localRotation = Quaternion.Euler(mouseYrot, 0, 0);

        //Rotate (X)
        transform.Rotate(Vector3.up * mouseX);
    }

    private void Movement()
    {
        //Get Inputs
        Vector3 movementInput = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 movement = (transform.right * movementInput.x + transform.forward * movementInput.z).normalized;

        //Make Move
        m_characterController.Move(movement.normalized * m_movementSpeed * Time.deltaTime);
    }

    private void GroundCheck()
    {
        m_isGrounded = Physics.CheckSphere(m_groundCheckTransform.position, m_groundCheckRadius, m_groundMask);
    }

    private void GravityAndJump()
    {
        //Are we touching ground?
        if (m_isGrounded == false)
        {
            m_verticalMovement.y += -m_gravity * Time.deltaTime;
        }
        else
        {
            if (Input.GetButtonDown("Jump"))
            {
                m_verticalMovement.y += m_jumpPower;
            }
            else
            {
                m_verticalMovement.y = 0;
            }
        }

        if (m_verticalMovement.magnitude != 0)
        {
            m_characterController.Move(m_verticalMovement * Time.deltaTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(m_groundCheckTransform.position, m_groundCheckRadius);
    }
}
