using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRB : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private bool m_isActive = true;
    [SerializeField] private Rigidbody m_rigidBody;

    [Header("Movement")]
    [SerializeField] private float m_movementSpeed;
    [SerializeField] private float m_jumpForce;

    [Header("Ground Check")]
    [SerializeField] private Transform m_groundCheckTransform;
    [SerializeField] private float m_groundCheckRadius;
    [SerializeField] private LayerMask m_groundMask;

    private bool m_isGrounded = false;

    private void Update()
    {
        if (m_isActive)
        {
            Movement();
            GroundCheck();
            GravityAndJump();
        }
    }

    private void Movement()
    {
        //Get Inputs
        Vector3 movementThisFrame = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Make Move
        transform.position += movementThisFrame * m_movementSpeed * Time.deltaTime;
    }

    private void GroundCheck()
    {
        m_isGrounded = Physics.CheckSphere(m_groundCheckTransform.position, m_groundCheckRadius, m_groundMask);
    }

    private void GravityAndJump()
    {
        //Are we touching ground?
        if (m_isGrounded == true && Input.GetButtonDown("Jump"))
        {
            m_rigidBody.AddForce(new Vector3(0, m_jumpForce, 0), ForceMode.Impulse);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(m_groundCheckTransform.position, m_groundCheckRadius);
    }
}