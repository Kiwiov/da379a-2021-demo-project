using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowXY : MonoBehaviour
{
    [SerializeField] private GameObject go;
    [SerializeField] private float height;

    void Update()
    {
        transform.position = new Vector3(go.transform.position.x, go.transform.position.y + height, go.transform.position.z);
    }
}
