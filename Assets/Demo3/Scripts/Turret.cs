using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField] private GameObject m_fork;
    [SerializeField] private GameObject m_gun;
    [SerializeField] private GameObject m_projectile;
    [SerializeField] private LayerMask m_turretLayerMask;
    [SerializeField] private Transform m_bulletSpawn;
    [SerializeField] private float m_fireDelay;

    private WaitForSeconds m_wait;
    private GameObject m_target;
    private Quaternion m_lookRotation;
    private Vector3 m_verticalPlusOne = new Vector3(0, 1, 0);
    private Vector3 m_hitPoint = Vector3.zero;

    void Start()
    {
        m_wait = new WaitForSeconds(m_fireDelay);
        StartCoroutine(AnyWaysSoIStartedBlasting()); 
    }

    
    void FixedUpdate()
    {
        if (m_target != null)
        {
            m_lookRotation = LookAt(m_gun.transform.position, m_target.transform.position + m_verticalPlusOne);

            Vector3 nextForkRotation = new Vector3(m_fork.transform.rotation.eulerAngles.x, m_lookRotation.eulerAngles.y, m_fork.transform.rotation.eulerAngles.z);
            m_fork.transform.rotation = Quaternion.Euler(nextForkRotation);

            Vector3 nextGunRotation = new Vector3(m_lookRotation.eulerAngles.x, m_fork.transform.rotation.eulerAngles.y, m_fork.transform.rotation.eulerAngles.z);
            m_gun.transform.rotation = Quaternion.Euler(nextGunRotation);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        RaycastHit hit;

        //Is it the player?
        if (other.tag == "Player")
        {
            Vector3 direction = other.transform.position + m_verticalPlusOne - m_gun.transform.position;
            direction.Normalize();

            if (Physics.Raycast(m_gun.transform.position, direction, out hit, 200, m_turretLayerMask))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    m_target = hit.collider.gameObject;
                }
                else
                {
                    m_target = null;
                }

                m_hitPoint = hit.point;
            }
        }
    }

    private IEnumerator AnyWaysSoIStartedBlasting()
    {
        while (true)
        {
            if (m_target != null)
            {
                Instantiate(m_projectile, m_bulletSpawn.position, m_bulletSpawn.rotation, transform);
                //pew pew
            }

            yield return m_wait;
        }
        
    }

    private void OnDrawGizmos()
    {
        if (m_target != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(m_gun.transform.position, m_target.transform.position + m_verticalPlusOne);
        }
        else
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(m_gun.transform.position, m_hitPoint);
        }
    }

    public static Quaternion LookAt(Vector3 sourcePoint, Vector3 destPoint)
    {
        Vector3 forwardVector = Vector3.Normalize(destPoint - sourcePoint);

        float dot = Vector3.Dot(Vector3.forward, forwardVector);

        if (Mathf.Abs(dot - (-1.0f)) < 0.000001f)
        {
            return new Quaternion(Vector3.up.x, Vector3.up.y, Vector3.up.z, 3.1415926535897932f);
        }
        if (Mathf.Abs(dot - (1.0f)) < 0.000001f)
        {
            return Quaternion.identity;
        }

        float rotAngle = (float)Mathf.Acos(dot);
        Vector3 rotAxis = Vector3.Cross(Vector3.forward, forwardVector);
        rotAxis = Vector3.Normalize(rotAxis);
        return CreateFromAxisAngle(rotAxis, rotAngle);
    }

    public static Quaternion CreateFromAxisAngle(Vector3 axis, float angle)
    {
        float halfAngle = angle * .5f;
        float s = (float)System.Math.Sin(halfAngle);
        Quaternion q;
        q.x = axis.x * s;
        q.y = axis.y * s;
        q.z = axis.z * s;
        q.w = (float)System.Math.Cos(halfAngle);
        return q;
    }
}
