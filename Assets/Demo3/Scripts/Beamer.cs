using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beamer : MonoBehaviour
{
    [SerializeField] private Transform m_origin;
    [SerializeField] private LayerMask m_targetedLayers;
    [SerializeField] private float m_delay;

    private WaitForSeconds m_wait;

    private void Start()
    {
        m_wait = new WaitForSeconds(m_delay);
        StartCoroutine(Shoot());
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            if (Physics.Raycast(m_origin.position, m_origin.forward, out RaycastHit hitInfo, 50.0f, m_targetedLayers))
            {
                if (hitInfo.transform.tag == "Player")
                {
                    Material playerMat = hitInfo.transform.gameObject.GetComponentInChildren<MeshRenderer>().material;
                    if (playerMat != null)
                    {
                    //HSV COLOR!!!
                        //Color pColor = playerMat.color;
                        //float h,s,v = 0;
                        //Color.RGBToHSV(pColor, out h, out s, out v);
                        //h += Random.value;
                        //h %= 1;
                        //playerMat.color = Color.HSVToRGB(h, s, v); 

                    //RGB COLOR!!!
                        Vector3 color = new Vector3(Random.value, Random.value, Random.value);
                        color.Normalize();
                        color /= 2;
                        playerMat.color = new Color(color.x, color.y, color.z, 1);
                    }
                }
            }

            yield return m_wait;
        }
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(m_origin.position, m_origin.forward);
    }
}
